const cookieNotice = document.cookie.split('; ').find(row => row.startsWith('cookieNotice'));

function afficherCookie() {
    if (cookieNotice != "cookieNotice=fermé") {
        creer_cookieNotice();
    }
    
}

function fermer() {
    let cookieNotice = document.getElementById('cookieNotice');
    document.body.removeChild(cookieNotice);

    document.cookie = "cookieNotice=fermé; max-age=604800; SameSite=Strict;";
}





function creer_cookieNotice() {
    let h3 = document.createElement('h3');
    h3.innerHTML = "Ce site utilise des cookies";

    let p = document.createElement('p');
    p.innerHTML = "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies pour enregistrer vos préférences.";

    let button = document.createElement('button');
    button.setAttribute('onclick', 'fermer()');
    button.innerHTML = "OK";

    let cookieNotice = document.createElement('div');
    cookieNotice.setAttribute('id', "cookieNotice");
    cookieNotice.appendChild(h3);
    cookieNotice.appendChild(p);
    cookieNotice.appendChild(button);

    document.body.appendChild(cookieNotice);
}