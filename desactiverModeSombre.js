function desactiverModeSombre() {
    document.getElementsByTagName('link')[2].href = "";

    document.querySelector("button").innerHTML = 'Activer Mode Sombre';
    document.querySelector("button").innerText = 'Activer Mode Sombre';
    document.querySelector("button").textContent = 'Activer Mode Sombre';
    document.querySelector("button").setAttribute('onclick',  'modeSombre()');

    document.cookie = "modeSombre=activé;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}
