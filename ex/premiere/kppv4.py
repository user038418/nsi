#####################################################################
# NSI
# Algorithme KPPV
#####################################################################
from kppv1 import freq_apparition_lettre
from kppv2 import distance
from kppv3 import index_plus_proche

#--------------- Texte en français -------------------
poeme = """C'est un trou de verdure où chante une rivière,
Accrochant follement aux herbes des haillons
D'argent ; où le soleil, de la montagne fière,
Luit : c'est un petit val qui mousse de rayons.

Un soldat jeune, bouche ouverte, tête nue,
Et la nuque baignant dans le frais cresson bleu,
Dort ; il est étendu dans l'herbe, sous la nue,
Pâle dans son lit vert où la lumière pleut.

Les pieds dans les glaïeuls, il dort. Souriant comme
Sourirait un enfant malade, il fait un somme :
Nature, berce-le chaudement : il a froid.

Les parfums ne font pas frissonner sa narine ;
Il dort dans le soleil, la main sur sa poitrine,
Tranquille. Il a deux trous rouges au côté droit.
"""

#--------------- Texte en anglais -------------------
poem = """Tyger Tyger, burning bright,
In the forests of the night;
What immortal hand or eye,
Could frame thy fearful symmetry?

In what distant deeps or skies,
Burnt the fire of thine eyes?
On what wings dare he aspire?
What the hand, dare seize the fire?

And what shoulder, & what art,
Could twist the sinews of thy heart?
And when thy heart began to beat,
What dread hand? & what dread feet?

What the hammer? what the chain,
In what furnace was thy brain?
What the anvil? what dread grasp,
Dare its deadly terrors clasp!

When the stars threw down their spears
And water'd heaven with their tears:
Did he smile his work to see?
Did he who made the Lamb make thee?

Tyger Tyger burning bright,
In the forests of the night:
What immortal hand or eye,
Dare frame thy fearful symmetry?
"""

#--------------- Texte en franglais -------------------
franglais = """J’en ai assez de servir de punching-ball pour mon boss.
C’est un chef style self-made-man qui a lancé sa start-up après avoir 
travaillé comme manager dans une chaîne de fast-food qui vend des 
hamburgers et des milk-shake. Il a tout quitté pour monter Miami Beach, 
c’est le nom de sa société. Je travaille pour lui depuis 2 ans et c’est 
pas top. Le salaire est bas, à la cantine (un self-service) ,  on nous 
propose toujours la même chose : steak frites ou hot dog.
Pour les vêtements, ce n’est pas la classe. Interdiction de porter le 
costume cravate, le look dans l’entreprise c’est plutôt jean, pull et 
baskets… j’ai même vu le patron une fois venir en jogging et sweat-shirt.
« Pourquoi être snob ? à Miami Beach, il faut être cool » C’est ce 
qu’il dit tout le temps. Mais bon, quand il y a un cocktail, il fait 
comme tout le monde, il met un smoking…
Enfin, ce week-end, je ne reste pas à Paris avec ses bars et ses 
night-clubs. Ah non. Je vais partir faire du camping, louer une 
caravane ou un bungalow et organiser un barbecue avec mes amis. Pas de 
soirée pizza DVD, pas de sorties cinéma pop-corn. Non, ce week-end 
c’est grillades, sandwiches et farniente.
"""

#-------------------- Base d'apprentissage ----------------------------
#-------- table contenant les fréquences d'apparition de U et H --------
# C'est une liste de tuples, chaque tuple correspond à un texte
# et contient (FreqU en %, FreqH en %, langue (A=Anglais, F=Français)
base_appr = [
    (5.61, 0.50, "F"),
    (2.99, 5.09, "A"),
    (5.28, 0.52, "F"),
    (2.57, 5.87, "A"),
    (6.91, 0.59, "F"),
    (2.68, 5.56, "A"),
    (5.06, 0.63, "F"),
    (2.91, 5.28, "A"),
    (3.09, 4.90, "A"),
    (5.75, 0.60, "F"),
    (3.12, 2.20, "A"),
]

def knn(base, k, texte):
    """
    Algorithme des knn
    arguments :
        base = base d'apprentissage (liste de tuples)
        k = nombres de voisins recherchés
        texte = texte pour lequel il faut reconnaitre la langue
    Valeur retournée :
        'A' (Anglais) ou 'F' (Français) 
    """
    #-----------------------------------------------------
    # calculer les fréquences de U et de H dans texte
    freq_u = freq_apparition_lettre(texte, 'U')
    freq_h = freq_apparition_lettre(texte, 'H')
    print("--------")
    print(f"fonction knn() : freqU = {freq_u}, freqH = {freq_h}")
    
    #-----------------------------------------------------
    # établir une liste de tuples (distance, donnée)
    liste = []
    for elem in base:
        dist = distance(freq_u, freq_h, elem[0], elem[1])
        liste.append((dist, elem))

    print("--------")
    print(f"fonction knn() : liste = {liste}")
    
    #-----------------------------------------------------
    # on recherche dans cette liste les k plus proches
    kppv = []
    for i in range(k):
        ipp = index_plus_proche(liste)

        kppv.append(liste.pop(ipp))

    print("--------")
    print(f"fonction knn() : kppv = {kppv}")
    
    # on compte le nombre de textes anglais et français
    nbfr = 0
    nben = 0
    for elem in kppv:
        if elem[1][2] == 'F':
            nbfr += 1
        elif elem[1][2] == 'A':
            nben += 1
        
    
    # on retourne 'A' ou 'F'
    if nbfr > nben:
        return 'F'
    if nben > nbfr:
        return 'A'


####################################################################
#                        PROGRAMME PRINCIPAL                       #
####################################################################

langue = knn(base_appr, 5, poeme)
print("\nlangue =", langue, "\n\n")

langue = knn(base_appr, 5, poem)
print("\nlangue =", langue, "\n\n")

langue = knn(base_appr, 5, franglais)
print("\nlangue =", langue, "\n\n")
