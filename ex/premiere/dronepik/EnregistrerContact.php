<?php
    // error_reporting(0);
    // ini_set("log_errors", 1);
    // ini_set("error_log", "erreurs_php.log");

    #Récupération de l'adresse IP pour le numéro client
    function recupAdresseIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }
        return hash('md5', $ip);
    }

    

    #Verification de l'origine de la requête pour éviter les erreurs
    if (!empty($_SERVER['HTTP_REFERER'])) {
        if ($_SERVER['HTTP_REFERER'] == "http://localhost/dronepik/contact.html") {
            ;
        } else {
            header("Location: contact.html");
        }
    } else {
        header("Location: contact.html");
    }


    #Récupération des données du fomulaire
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $commentaire = $_POST['commentaire'];
    $numClient = $_COOKIE['numeroClient'];

    #Récupération du numéro client
    if (!isset($numClient)) {
        $numClient = recupAdresseIp();
    }

    #Enregistrement des données entrées dans le formulaire de contact
    if ($nom != "" and $prenom != "" and $commentaire != "") {
        $fichierCommentaire = fopen("commentaires/$numClient.json", "a");

        $contenuCommentaire->nom = "$nom";
        $contenuCommentaire->prenom = "$prenom";
        $contenuCommentaire->commentaire = "$commentaire";

        fwrite($fichierCommentaire, json_encode($contenuCommentaire));

        fclose($fichierCommentaire);
    } else {
        header("Location: contact.html");
    }
?>

<html>
<head>
    <meta charset='utf-8'>
    <title>Dronépik - Contact</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
    <link rel="stylesheet" href="contact.css">
    <link rel="icon" href="https://image.flaticon.com/icons/svg/1610/1610133.svg">
</head>

<body onload="afficherCookies()">
        <div id="barreNav">
            <img src="images/logo.png" alt="">
            <nav>
                <a href="apropos.html">A Propos</a>
                <a class="active" href="contact.html">Contact</a>
                <a href="commande.php">Commander</a>
                <a href="modeles.html">Modèles</a>
                <a href="index.html">Accueil</a>
            </nav>
        </div>


    <div id="page">
        <h1>Formulaire de contact</h1>

        <h3>Merci de votre retour !</h3>
        
    </div>

    <footer>
        © Logo créé avec <a href="https://logomakr.com/">LogoMakr</a>, Icône de <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>.
    </footer>



    <div id="boite">
        <h2>Ce site utilise des cookies</h2>
        <p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour enregistrer votre panier et réaliser des statistiques de visites.</p>
        <button><a href="cookies.html">En savoir plus</a></button>
        <button onclick="fermer()">OK</button>
    </div>
</body>



<script>
    const boiteCookies = document.getElementById("boite");
    const cookieBaniere = document.cookie.split('; ').find(row => row.startsWith('baniereCookies'));

    //---------------Afficher la banière cookies---------------
    function afficherCookies() {
        if (cookieBaniere == "baniereCookies=cacher") {
            return;
        } else {
            boiteCookies.style.display = "block";
        }
    }

    //----------------Fermer la banière cookies----------------
    function fermer() {
        boiteCookies.style.display = "none";
        document.cookie = "baniereCookies=cacher;max-age=604800";
    }
</script>

</html>