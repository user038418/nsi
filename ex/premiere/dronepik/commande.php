<?php 
    ini_set("log_errors", 1);
    ini_set("error_log", "erreurs_php.log");

    
    if (isset($_COOKIE['numeroClient'])) {
        $numClient = $_COOKIE['numeroClient'];

        if (file_exists("commandes/$numClient.csv")) {
            header("Location: commandeVoir.php");
        }
    } else {
        $numClient = recupAdresseIp();

        if (file_exists("commandes/$numClient.csv")) {
            header("Location: commandeVoir.php");
        }
    }


    function recupAdresseIp(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }
        return hash('md5', $ip);
    }
?>


<html>
<head>
    <meta charset='utf-8'>
    <title>Dronépik - Commande</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
    <link rel="stylesheet" href="commande.css">
    <link rel="icon" href="images/shopping-cart.svg">
</head>

<body onload="afficherCookies()">
        <div id="barreNav">
            <img src="images/logo.png" alt="">
            <nav>
                <a href="apropos.html">A Propos</a>
                <a href="contact.html">Contact</a>
                <a class="active" href="commande.php">Commander</a>
                <a href="modeles.html">Modèles</a>
                <a href="index.html">Accueil</a>
            </nav>
        </div>


    <div id="page">
        <h1>Formulaire de commande</h1>

        <div id="progressBar">
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"><path d="M22 2v20h-20v-20h20zm2-2h-24v24h24v-24z"/></svg>
            <div class="ligne gris"></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"><path d="M22 2v20h-20v-20h20zm2-2h-24v24h24v-24z"/></svg>
            <div class="ligne gris"></div>
            <svg class="desactive" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
        </div>

        <form id="commande" action="commande2.php" method="POST">
            <section>
                <label for="modele">Modèle :</label>
                <select name="modele" required>
                    <option value=""></option>
                    <option value="Bourdon">Bourdon</option>
                    <option value="Mouche">Mouche</option>
                    <option value="Guêpe">Guêpe</option>
                    <option value="Moustique">Moustique</option>
                </select>
            </section>

            <section>
                <label>Résolution :</label>
                <div class="radio">
                    <input type="radio" name="resolution" id="p720" value="HD" checked>
                    <label for="p720">1280x720 pixels - HD</label>
                </div>
                <div class="radio">
                    <input type="radio" name="resolution" id="p1080" value="FullHD">
                    <label for="p1080">1920x1080 pixels - Full HD</label>
                </div>
                <div class="radio">
                    <input type="radio" name="resolution" id="p4K" value="4K">
                    <label for="p4K">4096x2160 pixels - 4K</label>
                </div>
            </section>

            <section>
                <label>Options :</label>
                <div class="check">
                    <input value="non" type="checkbox" id="bluetooth" name="bluetooth">
                    <input id="uncheckedBluetooth" type="hidden" value="non">
                    <label for="bluetooth">Connection bluetooth</label>
                </div>
                <div class="check">
                    <input value="non" type="checkbox" name="detecteurMouvement" id="detecteurMouvement">
                    <input id="uncheckedDetecteurMouvement" type="hidden" value="non">
                    <label for="detecteurMouvement">Detecteur de mouvements</label>
                </div>
                <div class="check">
                    <input value="non" type="checkbox" name="bourdonnement" id="bourdonnement">
                    <input id="uncheckedBourdonnement" type="hidden" value="non">
                    <label for="bourdonnement">Bourdonnement réaliste</label>
                </div>
            </section>

            <fieldset>
                <legend>Total</legend>
                <p>0 €</p>
                <input type="hidden" name="prix" value="0">
            </fieldset>


            <button id="enregistrer" type="submit">Suivant <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm2 12l-4.5 4.5 1.527 1.5 5.973-6-5.973-6-1.527 1.5 4.5 4.5z"/></svg></button>
        </form>
        


        <footer>
            © Logo créé avec <a href="https://logomakr.com/">LogoMakr</a>, Icône de <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>.
        </footer>
    </div>



    <div id="boite">
        <h2>Ce site utilise des cookies</h2>
        <p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour enregistrer votre panier et réaliser des statistiques de visites.</p>
        <button><a href="cookies.html">En savoir plus</a></button>
        <button onclick="fermer()">OK</button>
    </div>
</body>



<script>
    const boiteCookies = document.getElementById("boite");
    var prixTotal = 0;
    const formulaire = document.getElementsByTagName('form')[0];
    var modeleChoisi;
    const zoneTotal = document.getElementsByTagName('p')[0];
    const body = document.body;
    const options = document.getElementsByTagName('section')[2];
    const bluetooth = document.getElementById('bluetooth');
    const detecteurMouvement = document.getElementById('detecteurMouvement');
    const bourdonnement = document.getElementById('bourdonnement');
    const boiteDejaPasse = document.getElementById('dejaPasse');
    const cookieClient = document.cookie.split('; ').find(row => row.startsWith('numeroClient'));
    const cookieBaniere = document.cookie.split('; ').find(row => row.startsWith('baniereCookies'));


    //Mise à jour du total à chaque changement dans le formulaire
    formulaire.addEventListener('click', prix);

    //Mise à jour de 'value' des options
    formulaire.addEventListener('click', valueOptions);

    //Ajout numéro client
    formulaire.addEventListener('click', numeroClient);



    //--------------Ajout d'un identifiant client----------------
    function numeroClient() {
        if (cookieClient){
            formulaire.removeEventListener('change', numeroClient);
            return;
        } else {
            document.cookie = "numeroClient=" + "<?php echo recupAdresseIp() ?>" + ";max-age=604800;samesite=strict";
            formulaire.removeEventListener('change', numeroClient);
        }
    }
    
    

    //------------Modification 'value' des options---------------
    function valueOptions() {
        if(bluetooth.checked) {
            bluetooth.value = "oui";
            bluetooth.name = "bluetooth";
            
            document.getElementById('uncheckedBluetooth').name = "";
        }
        
        if(!bluetooth.checked) {
            document.getElementById('uncheckedBluetooth').name = "bluetooth";
            bluetooth.name = "";
        }
        
        
        if(detecteurMouvement.checked) {
            detecteurMouvement.value = "oui";
            detecteurMouvement.name = "detecteurMouvement";
            
            document.getElementById('uncheckedDetecteurMouvement').name = "";
        }
        
        if(!detecteurMouvement.checked) {
            document.getElementById('uncheckedDetecteurMouvement').name = "detecteurMouvement";
            detecteurMouvement.name = "";
        }
        
        
        if(bourdonnement.checked) {
            bourdonnement.value = "oui";
            bourdonnement.name = "bourdonnement";
            
            document.getElementById('uncheckedBourdonnement').name = "";
        }
        
        if(!bourdonnement.checked) {
            document.getElementById('uncheckedBourdonnement').name = "bourdonnement";
            bourdonnement.name = "";
        }
    }


    //-------------------Affichage du prix-----------------------
    function prix() {
        prixTotal = 0;

        
        for(var i in formulaire.elements["resolution"]) {
            if(formulaire.elements["resolution"][i].checked) {
                const resolution = formulaire.elements["resolution"][i].value;
                if (resolution == "HD") {
                    prixTotal = prixTotal + 0;
                } else if (resolution == "FullHD"){
                    prixTotal = prixTotal + 30;
                } else if (resolution == "4K"){
                    prixTotal = prixTotal + 60;
                }
            }
        }



        modeleChoisi = formulaire.elements["modele"].selectedIndex;
        if (modeleChoisi == 1) {
            prixTotal = prixTotal + 130;
        } else if (modeleChoisi == 2){
            prixTotal = prixTotal + 150;
        } else if (modeleChoisi == 3){
            prixTotal = prixTotal + 190;
        } else if (modeleChoisi == 4){
            prixTotal = prixTotal + 230;
        }


        if (bluetooth.checked) {
            prixTotal = prixTotal + 20;
        }

        if (detecteurMouvement.checked) {
            prixTotal = prixTotal + 20;
        }

        if (bourdonnement.checked) {
            prixTotal = prixTotal + 20;
        }


        if (!isNaN(prixTotal)) {
            zoneTotal.innerHTML = prixTotal + " €";
            formulaire.elements["prix"].value = prixTotal;
        }

        return;
    }


    //----------------Afficher la banière cookies----------------
    function afficherCookies() {
        if (cookieBaniere == "baniereCookies=cacher") {
            return;
        } else {
            boiteCookies.style.display = "block";
        }

        prix();
        valueOptions();
    }

    //-----------------Fermer la banière cookies-----------------
    function fermer() {
        boiteCookies.style.display = "none";
        document.cookie = "baniereCookies=cacher;max-age=604800";
    }
</script>

</html>
