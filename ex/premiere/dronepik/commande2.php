<?php 
    ini_set("log_errors", 1);
    ini_set("error_log", "erreurs_php.log");


    #Vérification de l'origine de la requête pour éviter les erreurs
    if (!empty($_SERVER['HTTP_REFERER'])) {
        if ($_SERVER['HTTP_REFERER'] == "http://localhost/dronepik/commande.php") {
            ;
        } else {
            header("Location: commande.php");
        }
    } else {
        header("Location: commande.php");
    }


    #Récupération de l'adresse IP pour le numéro client
    function recupAdresseIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }
        return hash('md5', $ip);
    }


    #Récupération des données du fomulaire
    $modele = $_POST['modele'];
    $resolution = $_POST['resolution'];
    $bluetooth = $_POST['bluetooth'];
    $detecteurMouvement = $_POST['detecteurMouvement'];
    $bourdonnement = $_POST['bourdonnement'];
    $prix = $_POST['prix'];
    $numClient = $_COOKIE['numeroClient'];

    if (!isset($numClient)) {
        $numClient = recupAdresseIp();
    }

    $numCommande = rand(10000000, 99999999); #Désignation d'un numéro de commande au hasard


    #Ajout des éléments de la commande au fichier de commande
    if ($modele != "" and $resolution != "" and $prix != "") {
        $fichierCommande = fopen("commandes/$numClient.csv", "a");
        if ($fichierCommande) {
            $contenuCommande = "$numCommande;$modele;$resolution;$bluetooth;$detecteurMouvement;$bourdonnement;$prix;";
            fputs($fichierCommande, $contenuCommande);

            fclose($fichierCommande);
        }
    } else {
        header('Location: commande.php'); #Retour à la page de commande principale si les champs necessaires ne sont pas remplis
    }
?>


<html>
<head>
    <meta charset='utf-8'>
    <title>Dronépik - Commande</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
    <link rel="stylesheet" href="commande.css">
    <link rel="icon" href="images/shopping-cart.svg">
</head>

<body onload="afficherCookies()">
        <div id="barreNav">
            <img src="images/logo.png" alt="">
            <nav>
                <a href="apropos.html">A Propos</a>
                <a href="contact.html">Contact</a>
                <a class="active" href="commande.php">Commander</a>
                <a href="modeles.html">Modèles</a>
                <a href="index.html">Accueil</a>
            </nav>
        </div>


    <div id="page">
        <h1>Formulaire de commande</h1>

        <div id="progressBar">
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
            <div class="ligne"></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"><path d="M22 2v20h-20v-20h20zm2-2h-24v24h24v-24z"/></svg>
            <div class="ligne gris"></div>
            <svg class="desactive" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
        </div>

        <form id="commande" action="commandeVoir.php" method="POST">
            <section>
                <label for="nom">Nom et Prénom :
                    <input type="text" name="nom" id="nom" placeholder="Nom" required>
                    <input type="text" name="prenom" id="prenom" placeholder="Prénom" required>
                </label>
            </section>

            <section>
                <label for="adresse">
                    Adresse :
                    <input type="text" name="adresse" id="adresse" placeholder="Adresse" required>
                </label>
                <label for="ville">
                    <input type="number" name="codePostal" id="CP" placeholder="Code Postal" required>
                    <input type="text" name="ville" id="ville" placeholder="Ville" required>
                </label>
            </section>

            <section>
                <label for="telephone">
                    Téléphone :
                    <input type="tel" name="telephone" minlength="10" maxlength="14" placeholder="Téléphone" required>
                </label>
            </section>

            <section>
                <label for="email">
                    Adresse Mail :
                    <input type="email" name="email" placeholder="exemple@email.com" required>
                </label>
            </section>

            <button id="enregistrer" type="submit">Suivant <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm2 12l-4.5 4.5 1.527 1.5 5.973-6-5.973-6-1.527 1.5 4.5 4.5z"/></svg></button>
            
        </form>


        <footer>
            © Logo créé avec <a href="https://logomakr.com/">LogoMakr</a>, Icône de <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>.
        </footer>
    </div>



    <div id="boite">
        <h2>Ce site utilise des cookies</h2>
        <p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour enregistrer votre panier et réaliser des statistiques de visites.</p>
        <button><a href="cookies.html">En savoir plus</a></button>
        <button onclick="fermer()">OK</button>
    </div>
</body>



<script>
    const boiteCookies = document.getElementById("boite");
    const formulaire = document.getElementsByTagName('form')[0];
    const body = document.body;
    const boiteDejaPasse = document.getElementById('dejaPasse');
    const cookieClient = document.cookie.split('; ').find(row => row.startsWith('numeroClient'));
    const cookieBaniere = document.cookie.split('; ').find(row => row.startsWith('baniereCookies'));
    const boutonSuivant = document.getElementById('enregistrer');


    //Ajout numéro commande
    document.cookie = "numeroCommande=" + "<?php echo $numCommande ?>" + ";max-age=604800;samesite=strict";

    //Ajout numéro client
    formulaire.addEventListener('load', numeroClient);
    //Enlever la signal de fermeture de page pour le bouton
    boutonSuivant.addEventListener("click", function() {window.onbeforeunload = null;});

    //Empêcher l'utilisateur de quitter la page
    window.onbeforeunload = function() {
            return "Si vous quittez maintenant votre commande sera incomplète, donc perdue.";
        };
    
    
    //--------------Ajout d'un identifiant client----------------
    function numeroClient() {
        if (cookieClient){
            formulaire.removeEventListener('load', numeroClient);
            return;
        } else {
            document.cookie = "numeroClient=" + "<?php echo recupAdresseIp() ?>" + ";max-age=604800;samesite=strict";
            formulaire.removeEventListener('load', numeroClient);
        }
    }


    //----------------Afficher la banière cookies----------------
    function afficherCookies() {
        if (cookieBaniere == "baniereCookies=cacher") {
            return;
        } else {
            boiteCookies.style.display = "block";
        }
    }

    //-----------------Fermer la banière cookies-----------------
    function fermer() {
        boiteCookies.style.display = "none";
        document.cookie = "baniereCookies=cacher;max-age=604800";
    }
</script>

</html>
